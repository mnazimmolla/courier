(function ($) {
    "use strict";
    jQuery(document).ready(function ($) {
        $('nav#dropdown').meanmenu({
            meanScreenWidth: "991",
            meanMenuContainer: ".mobile-menu-area .container",
        });
    });
    jQuery(window).load(function () {

    });
}(jQuery));
