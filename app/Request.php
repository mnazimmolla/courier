<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Request extends Model
{
    protected $fillable = ['order_id', 'merchant_id', 'runner_id', 'product_details', 'weight', 'pick_up', 'pick_up_address', 'destination', 'destination_address', 'expected_response', 'payment', 'status'];

    public static function checkMerchantExists()
    {
        $user_id = Auth::user()->id;
        $check = DB::table('requests')
            ->where('merchant_id','=',$user_id)
            ->get();

        if(count($check) > 0)
        {
            return true;
        }
        return false;
    }

    public static function merchantRequests()
    {
        $user_id = Auth::user()->id;
        $check = DB::table('requests')
            ->where('merchant_id','=',$user_id)
            ->orderBy('id', 'DESC')
            ->get();

        return $check;
    }

    public static function merchantRequest($merchant_id = null, $id = null)
    {
        $query = [
            'merchant_id'=> $merchant_id,
            'id' => $id
        ];
        $data = DB::table('requests')
            ->where($query)
            ->get();

        return $data;
    }

    public static function orders()
    {
        $data = DB::select(DB::raw("SELECT requests.*,users.phone FROM `requests` LEFT JOIN users ON requests.merchant_id=users.id WHERE requests.status=0 ORDER BY id DESC"));

        return $data;
    }

    public static function order($id = null)
    {
        $data = DB::select(DB::raw("SELECT requests.*,users.name FROM `requests` LEFT JOIN users ON requests.merchant_id=users.id WHERE requests.status=0 AND requests.id=$id"));
        return $data;

    }

    public static function totalRequest($id = null)
    {
        $data = DB::select(DB::raw("SELECT COUNT(merchant_id) as total FROM `requests` WHERE merchant_id=$id"));
        return $data;
    }

    public static function availableRequests($merchant_id = null ,$id = null)
    {
        $data = DB::select(DB::raw("SELECT COUNT(merchant_id) as available FROM `requests` WHERE merchant_id=$merchant_id and id=$id"));
        return $data;
    }

    public static function totalSpent($merchant_id = null)
    {
        $data = DB::select(DB::raw("SELECT SUM(payment) as spent FROM `requests` WHERE merchant_id=$merchant_id AND status = 2"));
        return $data;
    }

    public static function totalHire($merchant_id = null)
    {
        $data = DB::select(DB::raw("SELECT COUNT(status) AS hires FROM `requests` WHERE merchant_id = $merchant_id AND status = 2"));
        return $data;
    }

    public static function acceptOrder($user_id = null, $order_id = null)
    {

        DB::table('requests')
                    ->where('id', $order_id)
                    ->update(['runner_id' => $user_id,'status' => 1]);

    }

    public static function runnerOrderCompleted($id,$merchant_id)
    {
        $conditions = ['id' => $id, 'merchant_id' => $merchant_id];
        DB::table('requests')
            ->where($conditions)
            ->update(['status' => 2]);
    }

    public static function runnerAcceptedRequest($runner_id = null)
    {
        $conditions = ['runner_id' => $runner_id, 'status' => 1];
        $data = DB::table('requests')
            ->join('users','requests.merchant_id', '=', 'users.id')
            ->select('requests.*','users.id as merchant_ID','users.name')
            ->where($conditions)
            ->get();
        return $data;
    }

    public static function merchantProfile($id)
    {
        $data = DB::table('users')
            ->where('id',$id)
            ->select('name','username','phone','email','address','avater','created_at','updated_at')
            ->get();
        return $data;
    }

    public static function runnerCompleted($runner_id)
    {
        $conditions = ['runner_id'=>$runner_id,'status' => 2];
        $data = DB::table('requests')
            ->join('users','requests.merchant_id', '=', 'users.id')
            ->select('requests.*','users.id as merchant_ID','users.name')
            ->where($conditions)
            ->get();
        return $data;
    }

    public static function allAcceptedRequestForMerhchant($merchant_id)
    {
        $conditions = ['merchant_id' => $merchant_id,'status' => 1];
        $status = ['status' => 2];
        $data = DB::table('requests')
            ->join('users','requests.runner_id' , '=','users.id')
            ->select('requests.*','users.id AS runner_ID','users.name','users.avater','users.email','users.phone')
            ->where($conditions)
            ->orWhere($status)
            ->get();
        return $data;
    }

    public static function completedRequestForMerchant($merchant_id)
    {
        $conditions = ['merchant_id'=>$merchant_id,'status' => 2];
        $data = DB::table('requests')
            ->join('users','requests.runner_id', '=', 'users.id')
            ->select('requests.*','users.id as runner_ID','users.name')
            ->where($conditions)
            ->get();
        return $data;
    }

    public static function acceptedRunnerProfile($id)
    {
        $data = DB::table('users')
            ->where('id',$id)
            ->select('name','username','phone','email','address','avater','created_at','updated_at')
            ->get();
        return $data;
    }

    public static function runnerReviews($id)
    {
        $data = DB::select(DB::raw("SELECT COUNT(star) AS total FROM reviews,users WHERE users.id = reviews.runner_id AND reviews.runner_id = $id"));
        return $data;
    }

    public static function merchantConfirmOrder($order_id,$merchant_id)
    {
        $conditions = ['id' => $order_id, 'merchant_id' => $merchant_id];
        DB::table('requests')
            ->where($conditions)
            ->update(['status' => 3]);
    }

    public static function runnerInfo($runner_id)
    {
        $data = DB::table('users')
            ->where('id',$runner_id)
            ->get();
        return $data;
    }

    public static function searchService($pickup,$destination)
    {
        $conditions = [
            ['pick_up', 'LIKE', "%$pickup%"],
            ['requests.destination', 'LIKE', "%$destination%"],
        ];
        $query = DB::table('requests')
            ->join('users','requests.merchant_id', '=', 'users.id')
            ->where($conditions)
            ->select('requests.*','requests.id AS request_id','users.name','users.id','users.phone','users.email')
            ->get();
        return $query;
    }

    public static function searchRunner($pickup,$destination)
    {
        $conditions = [
            ['users.address', 'LIKE', "%$pickup%"],
            ['users.destination', 'LIKE', "%$destination%"],
        ];

        $query = DB::table('users')
            ->join('reviews', 'reviews.star', '=', 'users.id')
            ->where($conditions)
            ->select('users.name','users.phone','users.email','reviews.star')
            ->get();
    }

}
