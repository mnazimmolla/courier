<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Auth;

class MerchantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('template.layouts.merchant.profile');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('template.layouts.merchant.signup');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|min:5|max:30',
            'username' => 'required|min:5|max:20',
            'email' => 'required|email',
            'phone' => 'required|min:11|max:20',
            'address' => 'required',
            'password' => 'required|confirmed|min:6|max:64'
        ]);


        User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'address' => $data['address'],
            'password' => bcrypt($data['password']),
            'role' => $request->role,
        ]);
        Session::flash('merchant-signup-complete','Welcoome, You have successfully signed up.');
        return view('template.layouts.merchant.profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    public function show(Merchant $merchant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    public function edit(Merchant $merchant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user = User::find($id);
        $data = $request->validate([
            'name' => 'required|min:5|max:30',
            'email' => 'required|email|',
            'phone' => 'required|min:11|max:20',
        ]);

        if($request->hasFile('avater'))
        {
            $validate = $request->validate([
                'name' => 'required|min:5|max:30',
                'email' => 'required|email',
                'phone' => 'required|min:11|max:20',
                'avater' => 'mimes:jpeg,bmp,png|max:2048',
            ]);
            $avatar = $request->file('avater');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();

            $validate = [
                'name' => $request->name,
                'phone' => $request->phone,
                'email' => $request->email,
                'avater' => $filename,
            ];
            $user->update($validate);

            $request->avater->move(public_path('assets/img/profile/'), $filename);

        }

        $user->update($data);
        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Merchant $merchant)
    {
        //
    }

    public function login(Request $request)
    {
        if(Auth::attempt(['username'=>$request->username,'password'=>$request->password]))
        {

            if(Auth::user()->isMerchant())
            {
                return redirect('/merchant/profile');
            }
        }
        else
        {
            Session::flash('error', 'Invalid Username or Password');
            return redirect('/');
        }

    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

}
