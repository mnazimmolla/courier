<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Request as Runner;

class RunnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('template.layouts.runner.profile');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('template.layouts.runner.signup');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|min:5|max:30',
            'username' => 'required|min:5|max:20',
            'email' => 'required|email',
            'phone' => 'required|min:11|max:20',
            'address' => 'required',
            'destination' => 'required',
            'password' => 'required|confirmed|min:6|max:64'
        ]);


        User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'address' => $data['address'],
            'destination' => $data['destination'],
            'password' => bcrypt($data['password']),
            'role' => $request->role,
        ]);
        Session::flash('runner-signup-complete','You have successfully signed up.');
        return view('template.layouts.runner.profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $data = $request->validate([
            'name' => 'required|min:5|max:30',
            'email' => 'required|email|',
            'phone' => 'required|min:11|max:20',
        ]);

        if($request->hasFile('avater'))
        {
            $validate = $request->validate([
                'name' => 'required|min:5|max:30',
                'email' => 'required|email',
                'phone' => 'required|min:11|max:20',
                'avater' => 'mimes:jpeg,bmp,png|max:2048',
            ]);
            $avatar = $request->file('avater');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();

            $validate = [
                'name' => $request->name,
                'phone' => $request->phone,
                'email' => $request->email,
                'avater' => $filename,
            ];
            $user->update($validate);

            $request->avater->move(public_path('assets/img/profile/'), $filename);

        }

        $user->update($data);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function login(Request $request)
    {

        if(Auth::attempt(['username'=>$request->user_name,'password'=>$request->password]))
        {
            if(Auth::user()->isRunner())
            {
                return redirect('/runner/profile')->with('success','Welcome');
            }
            else
            {
                return redirect()->back();
            }
        }
        else
        {
            Session::flash('error', 'Invalid Username or Password');
            return redirect('/');
        }
    }

    public function searchRunnerForm()
    {
        return view('template.layouts.order.search-runner');
    }

    public function searchRunner(Request $request)
    {
        $pickup = $request->pick_up;
        $destination = $request->destination;
        $runners = Runner::searchRunner($pickup,$destination);

        return view('template.layouts.order.runner-results')->with('runners',$runners);
    }
}
