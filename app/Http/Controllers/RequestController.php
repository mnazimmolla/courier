<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use App\Request as ProductRequest;
use Illuminate\Http\Request;
use App\Location;
use Illuminate\Support\Facades\Session;

class RequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(ProductRequest::checkMerchantExists())
        {
            $location = Location::all();
            $data = ProductRequest::merchantRequests();
            $resources = [
                'location'=>$location,
                'data'=>$data,
            ];
            return view('template.layouts.merchant.pending-request')->with('resources', $resources);
        }
        else
        {
            Session::flash('empty', 'Your current request is 0!');
            return view('template.layouts.merchant.pending-request');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locations = Location::all();
        return view('template.layouts.merchant.make-request')->with('locations', $locations );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'pick_up' => 'required|max:50',
            'pick_up_address' => 'required|max:256|min:11',
            'destination' => 'required|max:50',
            'destination_address' => 'required|max:256|min:11',
            'expected_response' => 'required',
            'weight' => 'required',
            'payment' => 'required|integer',
            'product_details' => 'required'
        ]);

        $id = date('ymdhs').$request->merchant_id;
        ProductRequest::create([
            'pick_up' => $request->pick_up,
            'pick_up_address' => $request->pick_up_address,
            'destination' => $request->destination,
            'destination_address' => $request->destination_address,
            'expected_response' => $request->expected_response,
            'weight' => $request->weight,
            'payment' => $request->payment,
            'product_details' => $request->product_details,
            'merchant_id' => $request->merchant_id,
            'order_id' => $id,
        ]);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show($user_id, $id)
    {

        if(Auth::user()->id == $user_id)
        {
            $merchant_id = Auth::user()->id;
            $data = ProductRequest::merchantRequest($merchant_id,$id);

            if(count($data) > 0)
            {
                return view('template.layouts.merchant.edit-request')->with('request',$data[0]);
            }
            else
            {
                return view('template.layouts.merchant.edit-request');
            }


        }
        else
        {
            return redirect()->back();
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $requests = ProductRequest::find($id);

        $requests->update($data);

        return redirect('/merchant/pending-request');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function destroy($user_id, $id)
    {
        if(Auth::user()->id == $user_id)
        {
            $requests = ProductRequest::find($id);
            $requests->delete();
            return redirect()->back();
        }
        else
        {
            return redirect()->back();
        }
    }

    public function orders()
    {
        $data = ProductRequest::orders();
        return view('template.layouts.order.orders')->with('orders',$data);
    }

    public function order($merchant_id ,$id)
    {
        $order = ProductRequest::order($id);
        $total = ProductRequest::totalRequest($id);
        $available = ProductRequest::availableRequests($merchant_id,$id);
        $total_spent = ProductRequest::totalSpent($merchant_id);
        $total_hire = ProductRequest::totalHire($merchant_id);
        $order_details = [
            'order' => $order[0],
            'total' => $total[0],
            'available' => $available[0],
            'total_spent' => $total_spent[0],
            'total_hire' => $total_hire[0],
        ];

        return view('template.layouts.order.order')->with('order_details',$order_details);
    }

    public function acceptOrder($user_id = null, $order_id = null)
    {

        ProductRequest::acceptOrder($user_id,$order_id);
        return redirect('/runner/accepted-request');
    }

    public function runnerAccepted()
    {
        $accepted = ProductRequest::runnerAcceptedRequest(Auth::user()->id);

        return view('template.layouts.runner.accepted-request')->with('accepted',$accepted);
    }

    public function requestedMerchantProfile($id)
    {
        $merchant = ProductRequest::merchantProfile($id);

        return view('template.layouts.runner.merchant-profile')->with('merchant',$merchant[0]);
    }

    public function runnerCompleteRequests()
    {
        $completed = ProductRequest::runnerCompleted(Auth::user()->id);
        return view('template.layouts.runner.complete-request')->with('completed',$completed);
    }

    public function acceptRequests()
    {
        $accepted = ProductRequest::allAcceptedRequestForMerhchant(Auth::user()->id);
        return view('template.layouts.merchant.accepted-request')->with('allAccepted', $accepted);
    }

    public function merchantCompleteRequests()
    {
        $completed = ProductRequest::completedRequestForMerchant(Auth::user()->id);

        return view('template.layouts.merchant.complete-request')->with('completed',$completed);
    }

    public function acceptedRunnerProfile($id)
    {
        $runner = ProductRequest::acceptedRunnerProfile($id);
        $runner_reviews = ProductRequest::runnerReviews($id);

        $runner_detail = [
            'runner_profile' => $runner,
            'review' => $runner_reviews
        ];

        return view('template.layouts.merchant.runner-profile')->with('runner',$runner_detail);
    }

    public function runnerOrderComplete($order_id,$merchant_id)
    {
        $runnerComplete = ProductRequest::runnerOrderCompleted($order_id,$merchant_id);

        return redirect()->back();
    }

    public function merchantOrderCompleteConfirmation($order_id,$merchant_id)
    {
        $confirm = ProductRequest::merchantConfirmOrder($order_id,$merchant_id);

        return redirect()->back();
    }

    public function searchForRunnerForm()
    {
        return view('template.layouts.order.search-order');
    }

    public function searchForRunner(Request $request)
    {
        $pickup = $request->pick_up;
        $destination = $request->destination;
        $weight = $request->weight;
        $result = ProductRequest::searchService($pickup,$destination,$weight);
        return view('template.layouts.order.search-result')->with('results',$result);
    }

}
