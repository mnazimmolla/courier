<?php

namespace App;

use GuzzleHttp\Psr7\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','username','phone','email','address','destination','password','avater','role'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin($role = null)
    {
        if($this->role === 4 )
        {
            return true;
        }
        return false;
    }

    public function isModerator($role = null)
    {
        if($this->role === 3 )
        {
            return true;
        }
        return false;
    }

    public function isMerchant($role = null)
    {
        if($this->role === 2 )
        {
            return true;
        }
        return false;
    }

    public function isRunner($role = null)
    {
        if($this->role === 1 )
        {
            return true;
        }
        return false;
    }

}
