@include('template.layouts.partials.header')

{{--Development--}}
@yield('merchant-signup')
{{--Development--}}

@yield('merchant-profile')
@yield('merchant-make-request')
@yield('pending-request')
@yield('merchant-edit-request')
@yield('accepted-requested')
@yield('complete-request')
@yield('merchant-edit-profile')
@yield('merchant-index')
@yield('merchant-report')


@yield('order')
@yield('orders')

@yield('runner-signup')
@yield('runner-index')
@yield('runner-profile')
@yield('runner-edit-profile')
@yield('runner-accepted-request')
@yield('runner-available-request')
@yield('runner-review')
@yield('runner-complete')
@yield('runner-report')

@yield('make-review')

@yield('search-order')

@include('template.layouts.partials.footer')