<div class="dash-area">
    <div class="dashboard-area"><p>Welcome, <span>{{ Auth::user()->name }}</span></p></div>
    <div class="container-fluid">
        <div class="row">
            <div class="dash-padding">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="dash-nav">
                        <div class="list-group panel">
                            <a href="{{ url('/merchant/profile') }}" class="list-group-item">My Profile</a>
                        </div>
                    </div>
                </div>