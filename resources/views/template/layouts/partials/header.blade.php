<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Musco || Form</title>
    <link rel="shortcut icon" type="image/png" href="{{ asset('assets/img/favi.png') }}">

    <!-- all css here -->
    <!-- bootstrap v3.3.7 css -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/review.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/home.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/meanmenu.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet">

    <script src="{{asset('assets/js/vendor/modernizr-2.8.3.min.js')}}"></script>
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!-----header-top-area---->
<div class="all-body">

    <div class="header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-5 hidden-xs">
                    <div class="call-area">
                        <a href="#"><i class="fa fa-phone"></i> +91 654 123 784</a>
                        <a href="#"><i class="fa fa-envelope"></i> info@courier.com</a>
                    </div>
                </div>
                <div class="col-md-5 col-sm-3 col-xs-12">
                    <div class="location-area">
                        <a href="#"> <i class="fa fa-map-marker"></i>
                            Musco Courier, 12/2, Mirpur-1
                        </a>
                        @if(Session::has('error'))
                            <script>
                                alert('Invalid Username or Password');
                            </script>
                        @endif
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="our-sign-up-area">
                        @if(Auth::check())
                        <div class="v-profile">
                            <!-- Single button -->
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">

                                    @if(Auth::user()->avater == "0")
                                        <img src="{{ asset('assets/img/profile/avatar.png') }}" alt="User Avatar">
                                    @else
                                        <img src="{{ asset('assets/img/profile/'.Auth::user()->avater) }}" alt="User Avatar">
                                    @endif

                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">view profile</a></li>
                                    <li><a href="#">create request</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li><a href="{{ url('/merchant/logout') }}">log out</a></li>
                                </ul>
                            </div>
                        </div>


                        <div class="notificatin-area">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                    <span><i class="fa fa-globe"></i></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">view profile</a></li>
                                    <li><a href="#">create request</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li><a href="#">Message</a></li>
                                </ul>
                            </div>
                        </div>
                        @endif
                        @if(!Auth::check())

                        <div class="sign-up-area">
                            <ul>
                                <li><a>Signup</a>
                                    <ul>
                                        <li><a href="{{ url('/merchant/signup') }}">Beacome Merchant</a></li>
                                        <li><a href="{{ url('/runner/signup') }}">Beacome Runner</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="sign-up-area">
                            <ul>
                                <li><a>login</a>
                                    <ul>
                                        <li><a data-toggle="modal" data-target="#merchantModel" href="#">Merchant</a>
                                        </li>
                                        <li><a data-toggle="modal" data-target="#runnerModel" href="#">Runner</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                        @endif
                        {{--modal-for-merchant--}}
                        <div class="header-modal">
                        <div class="login-area">
                            <div class="modal fade" id="merchantModel" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="main-form">
                                                <div class="custom-log">
                                                    <span>Login</span>
                                                </div>
                                                <div class="form-body">
                                                    <form action="{{ url('merchant/login') }}" method="POST">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="username"
                                                                   id="username" placeholder="Username/E-mail/Phone" autocomplete="off">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="password" class="form-control" name="password"
                                                                   id="merchant_password" placeholder="Passward"/>
                                                        </div>
                                                        <div class="my-button">
                                                            <button type="submit" class="btn btn-default">Login</button>
                                                            <input type="hidden" name="_token" value="{{ Session::token() }}">
                                                            <input type="hidden" name="role" value="2">
                                                        </div>
                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--modal-for-merchant--}}
                        {{--modal-for-runner--}}
                        <div class="login-area">
                            <div class="modal fade" id="runnerModel" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="main-form">
                                                <div class="custom-log">
                                                    <span>Login</span>
                                                </div>
                                                <div class="form-body">
                                                    <form action="{{ url('/runner/login') }}" method="POST">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="user_name"
                                                                   id="user_name" placeholder="Username/E-mail/Phone" autocomplete="off" />
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="password" class="form-control" name="password"
                                                                   id="password" placeholder="Passward" autocomplete="off" />
                                                        </div>
                                                        <div class="my-button">
                                                            <button type="submit" class="btn btn-default">Login</button>
                                                            <input type="hidden" name="_token" value="{{ Session::token() }}">
                                                            <input type="hidden" name="role" value="1">
                                                        </div>
                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--modal-for-runner--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-----end-header-top-area---->
    <div class="menu-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="logo">
                        <a href="index.html"><img src="{{ asset('assets/img/main-logo.png') }}" alt=""></a>
                    </div>
                </div>
                <div class="col-md-9 col-sm-9 hidden-xs">
                    <div class="main-menu">
                        <nav>
                            <ul>
                                <li><a href="index.html">home</a></li>
                                <li><a href="servise.html">service <span class="caret"></span></a>
                                    <ul>
                                        <li><a href="index.html">Our Service</a></li>
                                        <li><a href="index.html">Service Area</a></li>
                                        <li><a href="index.html">Service Catagory</a></li>
                                        <li><a href="index.html">Service Delevery</a></li>
                                    </ul>
                                </li>
                                <li><a href="blog.html">faqs</a></li>
                                <li><a href="contact.html">contact</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!----menu-area-end---->


    <!----mobaile-menu-area---->
    <div class="mobile-menu-area visible-xs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                            <ul>
                                <li><a href="index.html">home</a></li>
                                <li><a href="servise.html">service <span class="caret"></span></a>
                                    <ul>
                                        <li><a href="index.html">Our Service</a></li>
                                        <li><a href="index.html">Service Area</a></li>
                                        <li><a href="index.html">Service Catagory</a></li>
                                        <li><a href="index.html">Service Delevery</a></li>
                                    </ul>
                                </li>
                                <li><a href="blog.html">faqs</a></li>
                                <li><a href="contact.html">contact</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!----mobaile-menu-area-end---->