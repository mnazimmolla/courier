<!-----footer-area---->
<div class="footer-area">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="footer-logo">
                    <a href="#"><img src="assets/img/main-logo.png" alt=""></a>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore </p>
                    <p>working hours:</p>
                    <p>Mon-sat 18:00 (Sunday off)</p>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-linkedin"></i></a>
                    <a href="#"><i class="fa fa-google"></i></a>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="footer-widget">
                    <span class="text-uppercase">GET IN touch</span>
                    <ul class="service-center">
                        <li><span><i class="fa fa-home"></i></span><a href="#"> 965/A, East Shewrapara, mirpur</a></li>
                        <li><span><i class="fa fa-globe"></i></span> <a href="#">www.Musco Courier.com</a></li>
                        <li><span><i class="fa fa-envelope"></i></span> <a href="#">info@email.com</a></li>
                        <li><span><i class="fa fa-phone"></i></span> <a href="#">01762-123 123</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-2 col-xs-12">
                <div class="footer-widget">
                    <span class="text-uppercase">services</span>
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About us</a></li>
                        <li><a href="#">Our works</a></li>
                        <li><a href="#">contact</a></li>
                        <li><a href="#">News</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="footer-widget">
                    <span class="text-uppercase">NEWSLETTER</span>
                    <P>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do </P>
                    <div class="form-group ">
                        <input type="text" class="form-control" placeholder="Enter Your Email">
                        <button class="btn subscribe">Subscribe</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script src="{{ asset('assets/js/vendor/jquery-1.12.4.min.js')  }}"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('assets/js/bootstrap.min.js')  }}"></script>
<script src="{{ asset('assets/js/owl.carousel.min.js')  }}"></script>
<script src="{{ asset('assets/js/main.js')  }}"></script>
</body>
</html>
