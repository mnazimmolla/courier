@extends('template.layouts.master')
@section('make-review')
    @include('template.layouts.merchant.sidebar')
    <!----Main Area---->
    <div class="col-md-9 col-sm-9 col-xs-12">
        {{--Page Name Header--}}
        <div class="dashboard-level">
            <div class="linkup">
                <ul>
                    <li><a href="#l">home</a></li>
                    <li><a href="#" class="active">blog</a></li>
                </ul>
            </div>
        </div>
        {{--Page Name Header--}}

        <div class="our-form">
            <div class="our-form-title text-center">
                <span>Review </span>
            </div>
            <div class="row">
                <div class="review-page col-md-5 col-xs-12 col-sm-3">
                    @if($runner['info']->first()->avater == "0")
                        <img src="{{ asset('assets/img/profile/avatar.png') }}" alt="User Avatar" class="img-circle">
                    @else
                        <img src="{{ asset('assets/img/profile/'.$runner['info']->first()->avater) }}" alt="User Avatar" class="img-circle">
                    @endif
                    <p class="review-name">{{ $runner['info']->first()->name }}</p>
                    <p>Runner</p>
                    <p>{{ $runner['info']->first()->email }}</p>
                </div>
                <div class="col-md-7 col-xs-12 col-sm-6">
                    <form class="comment" method="POST" action="{{ url('merchant/feedback') }}">
                        <fieldset class="rating">
                            <legend>Please rate:</legend>
                            <input type="radio" id="star5" name="rating" value="5" /><label for="star5" title="Outstanding">5 stars</label>
                            <input type="radio" id="star4" name="rating" value="4" /><label for="star4" title="Pretty Good">4 stars</label>
                            <input type="radio" id="star3" name="rating" value="3" /><label for="star3" title="Somehow Good">3 stars</label>
                            <input type="radio" id="star2" name="rating" value="2" /><label for="star2" title="Kinda Bad">2 stars</label>
                            <input type="radio" id="star1" name="rating" value="1"/><label for="star1" title="Sucks Big Time">1 star</label>
                        </fieldset>
                        <div class="form-group">
                            <textarea class="form-control" rows="6" cols="30" name="comment" id="comment" placeholder="Comment"></textarea>
                            <button type="submit" class="btn brand-btn">Submit</button>
                            <input type="hidden" name="_token" value="{{ Session::token() }}">
                            <input type="hidden" name="order_id" value="{{ $runner['order_id'] }}">
                            <input type="hidden" name="merchant_id" value="{{ $runner['merchant_id'] }}">
                            <input type="hidden" name="runner_id" value="{{ $runner['info']->first()->id }}">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!----Main Area---->
    </div>
    </div>
    </div>
    </div>
@endsection

































