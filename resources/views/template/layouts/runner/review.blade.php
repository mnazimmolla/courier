@extends('template.layouts.master')
@section('runner-available-request')
    @include('template.layouts.runner.sidebar')
    <!----Main Area---->
    <div class="col-md-9 col-sm-9 col-xs-12">
        {{--Page Name Header--}}
        <div class="dashboard-level">
            <div class="linkup">
                <ul>
                    <li><a href="#l">home</a></li>
                    <li><a href="#" class="active">blog</a></li>
                </ul>
            </div>
        </div>
        {{--Page Name Header--}}
        <div class="main-table-area">
            <table class="table-striped">
                <tr>
                    <th>Order No</th>
                    <th>Pickup Point</th>
                    <th>Destination</th>
                    <th>Weight</th>
                    <th>Payment</th>
                    <th>Status</th>
                </tr>
                <tr>
                    <td>#481</td>
                    <td>Mohammadpur</td>
                    <td>Newmarket</td>
                    <td>75 KG</td>
                    <td>100 BDT</td>
                    <td><button type="button" class="btn btn-info">Accept</button></td>
                </tr>
                <tr>
                    <td>#481</td>
                    <td>Mohammadpur</td>
                    <td>Newmarket</td>
                    <td>75 KG</td>
                    <td>100 BDT</td>
                    <td><button type="button" class="btn btn-info">Accept</button></td>
                </tr>
                <tr>
                    <td>#481</td>
                    <td>Mohammadpur</td>
                    <td>Newmarket</td>
                    <td>75 KG</td>
                    <td>100 BDT</td>
                    <td><button type="button" class="btn btn-info">Accept</button></td>
                </tr>
                <tr>
                    <td>#481</td>
                    <td>Mohammadpur</td>
                    <td>Newmarket</td>
                    <td>75 KG</td>
                    <td>100 BDT</td>
                    <td><button type="button" class="btn btn-info">Accept</button></td>
                </tr>
            </table>
        </div>
    </div>
    <!----Main Area---->
    </div>
    </div>
    </div>
    </div>
@endsection














































































































@extends('template.layouts.master')
@section('runner-review')
    @include('template.layouts.runner.sidebar')
    <div class="review-area">
        <div class="container">
            <div class="row">
                <div class="dashboard-level">
                    <div class="linkup">
                        <ul>
                            <li><a href="#l">home</a></li>
                            <li><a href="#" class="active">blog</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-5 col-md-offset-2">
                    <div class="main-review">
                        <div class="review-img ">
                            <img class="img-circle" src="assets/img/images.png" alt="">
                        </div>
                        <div class="review-details">
                            <p>Ratul Ahmed</p>
                            <p>
                                <span>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                               </span>
                            </p>
                            <p>Lorem ipsum dolor sit amet.</p>
                            <p>5 Days Ago</p>
                            <p>Date: 30.02.1915</p>
                        </div>
                    </div>
                    <hr>
                    <div class="main-review">
                        <div class="review-img ">
                            <img class="img-circle" src="assets/img/images.png" alt="">
                        </div>
                        <div class="review-details">
                            <p>Ratul Ahmed</p>
                            <p>
                                <span>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                               </span>
                            </p>
                            <p>Lorem ipsum dolor sit amet.</p>
                            <p>5 Days Ago</p>
                            <p>Date: 30.02.1915</p>
                        </div>
                    </div>
                    <hr>
                    <div class="main-review">
                        <div class="review-img ">
                            <img class="img-circle" src="assets/img/images.png" alt="">
                        </div>
                        <div class="review-details">
                            <p>Ratul Ahmed</p>
                            <p>
                                <span>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                               </span>
                            </p>
                            <p>Lorem ipsum dolor sit amet.</p>
                            <p>5 Days Ago</p>
                            <p>Date: 30.02.1915</p>
                        </div>
                    </div>
                    <hr>
                    <div class="main-review">
                        <div class="review-img ">
                            <img class="img-circle" src="assets/img/images.png" alt="">
                        </div>
                        <div class="review-details">
                            <p>Ratul Ahmed</p>
                            <p>
                                <span>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                               </span>
                            </p>
                            <p>Lorem ipsum dolor sit amet.</p>
                            <p>5 Days Ago</p>
                            <p>Date: 30.02.1915</p>
                        </div>
                    </div>
                    <hr>
                    <div class="main-review">
                        <div class="review-img ">
                            <img class="img-circle" src="assets/img/images.png" alt="">
                        </div>
                        <div class="review-details">
                            <p>Ratul Ahmed</p>
                            <p>
                                <span>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                               </span>
                            </p>
                            <p>Lorem ipsum dolor sit amet.</p>
                            <p>5 Days Ago</p>
                            <p>Date: 30.02.1915</p>
                        </div>
                    </div>
                    <hr>
                </div>
            </div>
        </div>
    </div>
@endsection