@extends('template.layouts.master')
@section('runner-profile')
@include('template.layouts.runner.sidebar')
    <!----Main Area---->
    <div class="col-md-9 col-sm-9 col-xs-12">
        {{--Page Name Header--}}
        <div class="dashboard-level">
            <div class="linkup">
                <ul>
                    <li><a href="#l">home</a></li>
                    <li><a href="#" class="active">blog</a></li>
                    @if(Session::has('runner-signup-complete'))
                        <p class="alert alert-info">{{ Session::get('runner-signup-complete') }}</p>
                    @endif
                </ul>
            </div>
        </div>
        {{--Page Name Header--}}
        <div class="main-table-area">
            <div class="main-overview">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <div class="overview-img">
                            @if(Auth::user()->avater == "0")
                            <img src="{{ asset('assets/img/profile/avatar.png') }}" alt="User Avatar" class="thumbnail">
                            @else
                                <img src="{{ asset('assets/img/profile/'.Auth::user()->avater) }}" alt="User Avatar" class="thumbnail">
                            @endif
                            <div class="overview-title">
                                @if(Auth::check())
                                    <span>{{ Auth::user()->name }}</span>
                                    <p>Joined : {{ Auth::user()->created_at->diffForHumans() }}</p>
                                    <p>Updated : {{ Auth::user()->updated_at->diffForHumans() }}</p>
                                    <p>Email : {{ Auth::user()->email }}</p>
                                    <p>Phone : 0{{ Auth::user()->phone }}</p>
                                    <p><a href="#" data-toggle="modal" data-target="#edit_profile">Edit Profile</a></p>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-7 col-sm-7 col-xs-12">
                        <div class="overview-details row">
                            <div class="">
                                <div class="overview-img-details">
                                    <ul>
                                        <li><i class="fa fa-briefcase"></i>Project <span>10</span></li>
                                        <li><i class="fa fa-envelope"></i>Messages <span>12</span></li>
                                        <li><i class="fa fa-user"></i>Friends <span>15</span></li>
                                        <li><i class="fa fa-cog"></i>setting</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="">
                                <div class="overview-summery">
                                    <span>Summeary</span>
                                    <ul>
                                        <li>Today Sold <span>12</span></li>
                                        <li>Weekly Sales <span>50</span></li>
                                        <li>Total Sales <span>350</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="overview-icon row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="over-icon-area">
                                    <ul>
                                        <li><a href=""><i class="fa fa-google"></i> @sm</a></li>
                                        <li><a href=""><i class="fa fa-twitter"></i> t_sm</a></li>
                                        <li><a href=""><i class="fa fa-facebook"></i> t_sm</a></li>
                                        <li><a href=""><i class="fa fa-skype"></i> t_sm</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!----Main Area---->

    {{--Edit-Profile-Modal--}}
    <!-- Modal -->
    <div id="edit_profile" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Profile</h4>
                </div>

                <div class="modal-body">
                    @if(Auth::check())
                    <form action="{{url('/runner/profile/update/'.Auth::user()->id)}}" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" id="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ Auth::user()->name }}"/>
                            @if ($errors->has('name'))
                                <span class="text-danger">
                                        <strong>{{ $errors->first('name') }}</strong>
                                 </span>
                            @endif
                            <label for="email">E-mail</label>
                            <input type="email" name="email" id="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ Auth::user()->email }}" />
                            @if ($errors->has('email'))
                                <span class="text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                 </span>
                            @endif
                            <label for="phone">Phone</label>
                            <input type="phone" name="phone" id="phone" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" value="0{{ Auth::user()->phone }}" />
                            @if ($errors->has('phone'))
                                <span class="text-danger">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                 </span>
                            @endif
                            <label for="avatar">Profile Avatar</label>
                            <input type="file" name="avater" id="avatar" class="form-control{{ $errors->has('avatar') ? ' is-invalid' : '' }}" />
                            @if ($errors->has('avatar'))
                                <span class="text-danger">
                                        <strong>{{ $errors->first('avatar') }}</strong>
                                 </span>
                            @endif
                        </div>
                        <input type="hidden" name="_token" id="" value="{{Session::token()}}">
                        <button type="submit" class="btn">Update</button>
                    </form>
                    @endif
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    {{--Edit-Profile-Modal--}}

    </div>
    </div>
    </div>
    </div>




@endsection
