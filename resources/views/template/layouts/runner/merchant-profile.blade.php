@extends('template.layouts.master')
@section('runner-profile')
    @include('template.layouts.runner.sidebar')
    <!----Main Area---->
    <div class="col-md-9 col-sm-9 col-xs-12">
        {{--Page Name Header--}}
        <div class="dashboard-level">
            <div class="linkup">
                <ul>
                    <li><a href="#l">home</a></li>
                    <li><a href="#" class="active">blog</a></li>
                </ul>
            </div>
        </div>
        {{--Page Name Header--}}
        <div class="main-table-area">
            <div class="main-overview">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <div class="overview-img">
                            @if($merchant->avater == "0")
                                <img src="{{ asset('assets/img/profile/avatar.png') }}" alt="User Avatar" class="thumbnail">
                            @else
                                <img src="{{ asset('assets/img/profile/'.$merchant->avater) }}" alt="User Avatar" class="thumbnail">
                            @endif
                            <div class="overview-title">
                                @if(Auth::check())
                                    <span>{{ $merchant->name }}</span>
                                    <p>Joined : {{ $merchant->created_at }}</p>
                                    <p>Updated : {{ $merchant->updated_at }}</p>
                                    <p>Email : {{ $merchant->email }}</p>
                                    <p>Phone : 0{{ $merchant->phone }}</p>
                                @endif
                            </div>
                        </div>
                    </div>



                    <div class="col-md-7 col-sm-7 col-xs-12">
                        <div class="overview-details row">
                            <div class="">
                                <div class="overview-img-details">
                                    <ul>
                                        <li><i class="fa fa-briefcase"></i>Project <span>10</span></li>
                                        <li><i class="fa fa-envelope"></i>Messages <span>12</span></li>
                                        <li><i class="fa fa-user"></i>Friends <span>15</span></li>
                                        <li><i class="fa fa-cog"></i>setting</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="">
                                <div class="overview-summery">
                                    <span>Summeary</span>
                                    <ul>
                                        <li>Today Sold <span>12</span></li>
                                        <li>Weekly Sales <span>50</span></li>
                                        <li>Total Sales <span>350</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="overview-icon row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="over-icon-area">
                                    <ul>
                                        <li><a href=""><i class="fa fa-google"></i> @sm</a></li>
                                        <li><a href=""><i class="fa fa-twitter"></i> t_sm</a></li>
                                        <li><a href=""><i class="fa fa-facebook"></i> t_sm</a></li>
                                        <li><a href=""><i class="fa fa-skype"></i> t_sm</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!----Main Area---->

    </div>
    </div>
    </div>
    </div>




@endsection
