@extends('template.layouts.master')
@section('runner-accepted-request')
    @include('template.layouts.runner.sidebar')
    <!----Main Area---->
    <div class="col-md-9 col-sm-9 col-xs-12">
        {{--Page Name Header--}}
        <div class="dashboard-level">
            <div class="linkup">
                <ul>
                    <li><a href="#l">home</a></li>
                    <li><a href="#" class="active">blog</a></li>
                </ul>
            </div>
        </div>
        {{--Page Name Header--}}
        <div class="main-table-area">
            <table class="table table-striped">
                <tr>
                    <th>Order No</th>
                    <th>Name</th>
                    <th>Date</th>
                    <th>Weight</th>
                    <th>Pickup Point</th>
                    <th>Pickup Time</th>
                    <th>Destination</th>
                    <th>Payment</th>
                    <th>Status</th>
                </tr>
                @if(count($accepted) <= 0 )
                <p class="alert alert-danger">Currently You have no Accepted Request.</p>
                @endif
                @foreach($accepted as $accept)
                    <tr>
                        <td>#{{ $accept->order_id }}</td>
                        <td> <a href="{{ url('/requests/merchant/'.$accept->merchant_ID) }}">{{ $accept->name }}</a></td>
                        <td>{{ $accept->created_at }}</td>
                        <td>{{ $accept->weight }}</td>
                        <td>{{ $accept->pick_up }}</td>
                        <td>{{ $accept->expected_response }}</td>
                        <td>{{ $accept->destination }}</td>
                        <td>{{ $accept->payment }}</td>
                        <td>Ongoing
                            @if($accept->status == 1)
                            <a href="{{ url('order/completed/runner/order_id/'.$accept->id.'/merchant_id/'.$accept->merchant_ID) }}" title="Click Complete Button If Product has been Successfully Delivered">Complete</a>
                            @endif
                        </td>

                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    <!----Main Area---->
    </div>
    </div>
    </div>
    </div>
@endsection