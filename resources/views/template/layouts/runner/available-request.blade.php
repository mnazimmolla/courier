@extends('template.layouts.master')
@section('runner-available-request')
    @include('template.layouts.runner.sidebar')
    <!----Main Area---->
    <div class="col-md-9 col-sm-9 col-xs-12">
        {{--Page Name Header--}}
        <div class="dashboard-level">
            <div class="linkup">
                <ul>
                    <li><a href="#l">home</a></li>
                    <li><a href="#" class="active">blog</a></li>
                </ul>
            </div>
        </div>
        {{--Page Name Header--}}
        <div class="main-table-area">
            <table class="table-striped">
                <tr>
                    <th>Order No</th>
                    <th>Pickup Point</th>
                    <th>Destination</th>
                    <th>Weight</th>
                    <th>Payment</th>
                    <th>Status</th>
                </tr>
                <tr>
                    <td>#481</td>
                    <td>Mohammadpur</td>
                    <td>Newmarket</td>
                    <td>75 KG</td>
                    <td>100 BDT</td>
                    <td><button type="button" class="btn btn-info">Accept</button></td>
                </tr>
                <tr>
                    <td>#481</td>
                    <td>Mohammadpur</td>
                    <td>Newmarket</td>
                    <td>75 KG</td>
                    <td>100 BDT</td>
                    <td><button type="button" class="btn btn-info">Accept</button></td>
                </tr>
                <tr>
                    <td>#481</td>
                    <td>Mohammadpur</td>
                    <td>Newmarket</td>
                    <td>75 KG</td>
                    <td>100 BDT</td>
                    <td><button type="button" class="btn btn-info">Accept</button></td>
                </tr>
                <tr>
                    <td>#481</td>
                    <td>Mohammadpur</td>
                    <td>Newmarket</td>
                    <td>75 KG</td>
                    <td>100 BDT</td>
                    <td><button type="button" class="btn btn-info">Accept</button></td>
                </tr>
            </table>
        </div>
    </div>
    <!----Main Area---->
    </div>
    </div>
    </div>
    </div>
@endsection