<div class="dash-area">
    @if(Auth::check())
    <div class="dashboard-area"><p>Welcome, <span>{{ Auth::user()->name }}</span></p></div>
    @endif
    <div class="container-fluid">
        <div class="row">
            <div class="dash-padding">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="dash-nav">
                        <div class="list-group panel">
                            <a href="{{ url('/runner/profile') }}" class="list-group-item">My Profile</a>
                        </div>
                        <div class="list-group panel">
                            <a href="{{ url('runner/accepted-request/') }}" class="list-group-item">Accepted
                                Requests</a>
                        </div>
                        <div class="list-group panel">
                            <a href="{{ url('/orders') }}" class="list-group-item">Available
                                Requests</a>
                        </div>
                        <div class="list-group panel">
                            <a href="{{ url('runner/complete-request/') }}" class="list-group-item">Complete
                                Requests</a>
                        </div>
                        <div class="list-group panel">
                            <a href="{{ url('runner/reviews') }}" class="list-group-item">Reviews</a>
                        </div>
                        <div class="list-group panel">
                            <a href="#" class="list-group-item">Report to Admin</a>
                        </div>
                    </div>
                </div>