@extends('template.layouts.master')
@section('runner-index')
@include('template.layouts.runner.sidebar')

    <!----Main Area---->
    <div class="col-md-9 col-sm-9 col-xs-12">
        {{--Page Name Header--}}
        <div class="dashboard-level">
            <div class="linkup">
                <ul>
                    <li><a href="#l">home</a></li>
                    <li><a href="#" class="active">blog</a></li>
                </ul>
            </div>
        </div>
        {{--Page Name Header--}}
        <div class="main-table-area">
            <table class="table table-striped">
                <tr>
                    <th>Order ID</th>
                    <th>Pickup Point</th>
                    <th>Destination</th>
                    <th>Weight</th>
                    <th>Accepted by</th>
                    <th>Payment</th>
                    <th>Status</th>
                </tr>
                <tr>
                    <td>#320dfsasdfsldfas</td>
                    <td>Mohammadpur</td>
                    <td>Newmarket</td>
                    <td>240 KG</td>
                    <td><a href="#">Ratul Ahmed</td>
                    <td>100 BDT</td>
                    <td>Complete</td>
                </tr>
                <tr>
                    <td>#320dfsasdfsldfas</td>
                    <td>Mohammadpur</td>
                    <td>Newmarket</td>
                    <td>240 KG</td>
                    <td><a href="#">Ratul Ahmed</td>
                    <td>100 BDT</td>
                    <td>Complete</td>
                </tr>
                <tr>
                    <td>#320dfsasdfsldfas</td>
                    <td>Mohammadpur</td>
                    <td>Newmarket</td>
                    <td>240 KG</td>
                    <td><a href="#">Ratul Ahmed</td>
                    <td>100 BDT</td>
                    <td>Complete</td>
                </tr>
                <tr>
                    <td>#320dfsasdfsldfas</td>
                    <td>Mohammadpur</td>
                    <td>Newmarket</td>
                    <td>240 KG</td>
                    <td><a href="#">Ratul Ahmed</td>
                    <td>100 BDT</td>
                    <td>Complete</td>
                </tr>
                <tr>
                    <td>#320dfsasdfsldfas</td>
                    <td>Mohammadpur</td>
                    <td>Newmarket</td>
                    <td>240 KG</td>
                    <td><a href="#">Ratul Ahmed</td>
                    <td>100 BDT</td>
                    <td>Complete</td>
                </tr>
            </table>
        </div>
    </div>
    <!----Main Area---->
    </div>
    </div>
    </div>
    </div>
@endsection