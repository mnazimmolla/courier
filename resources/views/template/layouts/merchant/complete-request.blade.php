@extends('template.layouts.master')
@section('complete-request')
    @include('template.layouts.merchant.sidebar')
    <!----Main Area---->
    <div class="col-md-9 col-sm-9 col-xs-12">
        {{--Page Name Header--}}
        <div class="dashboard-level">
            <div class="linkup">
                <ul>
                    <li><a href="#l">home</a></li>
                    <li><a href="#" class="active">blog</a></li>
                </ul>
            </div>
        </div>
        {{--Page Name Header--}}
        <div class="main-table-area">
            <table class="table table-striped">
                <tr>
                    <th>Order ID</th>
                    <th>Pickup Point</th>
                    <th>Destination</th>
                    <th>Weight</th>
                    <th>Accepted by</th>
                    <th>Payment</th>
                    <th>Status</th>
                </tr>
                @if(count($completed) <= 0)
                <p class="alert alert-danger">You have no Complete Request.</p>
                @endif
                @foreach($completed as $complete)
                <tr>
                    <td>#{{ $complete->order_id }}</td>
                    <td>{{ $complete->pick_up }}</td>
                    <td>{{ $complete->destination }}</td>
                    <td>{{ $complete->weight }}</td>
                    <td><a href="{{ url('/requests/runner/'.$complete->runner_ID) }}">{{ $complete->name }}</a></td>
                    <td>{{ $complete->payment }}</td>
                    <td>Complete</td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
    <!----Main Area---->
    </div>
    </div>
    </div>
    </div>
@endsection