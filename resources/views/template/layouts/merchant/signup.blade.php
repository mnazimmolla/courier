@extends('template.layouts.master')
@section('merchant-signup')
    @include('template.layouts.merchant.sidebar')
    <!----Main Area---->
    <div class="col-md-9 col-sm-9 col-xs-12">
        {{--Page Name Header--}}
        <div class="dashboard-level">
            <div class="linkup">
                <ul>
                    <li><a href="#l">home</a></li>
                    <li><a href="#" class="active">blog</a></li>
                </ul>
            </div>
        </div>
        {{--Page Name Header--}}

        <div class="our-form">
            <div class="our-form-title text-center">
                <span>Merchant Signup</span>
            </div>
            <div class="row">
                <div class="col-md-3 col-xs-12 col-sm-3"></div>
                <div class="col-md-6 col-xs-12 col-sm-6">
                    <form action="{{ url('/merchant/signup') }}" method="POST">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" id="name" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Name">
                            @if ($errors->has('name'))
                                <span class="text-danger">
                                        <strong>{{ $errors->first('name') }}</strong>
                                 </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" id="username" name="username" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" placeholder="Username">
                            @if ($errors->has('username'))
                                <span class="text-danger">
                                        <strong>{{ $errors->first('username') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="email">E-mail Address</label>
                            <input type="email" id="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="E-mail">
                            @if ($errors->has('email'))
                                <span class="text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="number" id="phone" name="phone" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" placeholder="Phone">
                            @if ($errors->has('phone'))
                                <span class="text-danger">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="address">Permanent Address / Office </label>
                            <textarea class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" id="address" name="address" rows="3" placeholder="Address"></textarea>
                            @if ($errors->has('address'))
                                <span class="text-danger">
                                        <strong>{{ $errors->first('address') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="password">Password </label>
                            <input type="password" id="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="text-danger">
                                        <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">Confirm Password </label>
                            <input type="password" id="password_confirmation" name="password_confirmation" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="text-danger">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                        <input type="hidden" name="_token" id="_token" value="{{ Session::token() }}">
                        <input type="hidden" name="role" value="2">
                        <button type="submit" class="btn btn-info">Submit</button>
                    </form>
                </div>
                <div class="col-md-3 col-xs-12 col-sm-3"></div>
            </div>
        </div>
    </div>
    <!----Main Area---->
    </div>
    </div>
    </div>
    </div>
@endsection
