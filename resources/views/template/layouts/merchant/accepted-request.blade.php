@extends('template.layouts.master')
@section('accepted-requested')
@include('template.layouts.merchant.sidebar')
<!----Main Area---->
<div class="col-md-9 col-sm-9 col-xs-12">
    {{--Page Name Header--}}
    <div class="dashboard-level">
        <div class="linkup">
            <ul>
                <li><a href="#l">home</a></li>
                <li><a href="#" class="active">blog</a></li>
                @if(Session::has('greet'))
                <p class="alert alert-info">{{ Session::get('greet') }}</p>
                @endif
            </ul>
        </div>
    </div>
    {{--Page Name Header--}}
    <div class="main-table-area">
        <table class="table table-striped">
            <tr>
                <th>Order No</th>
                <th>Name</th>
                <th>Date</th>
                <th>Weight</th>
                <th>Pickup Point</th>
                <th>Pickup Time</th>
                <th>Destination</th>
                <th>Payment</th>
                <th>Status</th>
            </tr>
            @if(count($allAccepted) <= 0 )
                <p class="alert alert-danger">Currently You have no Accepted Request.</p>
            @endif
            @foreach($allAccepted as $accept)
                <tr>
                    <td>#{{ $accept->order_id }}</td>
                    <td> <a href="{{ url('/requests/merchant/'.$accept->runner_ID) }}">{{ $accept->name }}</a></td>
                    <td>{{ $accept->created_at }}</td>
                    <td>{{ $accept->weight }}</td>
                    <td>{{ $accept->pick_up }}</td>
                    <td>{{ $accept->expected_response }}</td>
                    <td>{{ $accept->destination }}</td>
                    <td>{{ $accept->payment }}</td>
                    <td>Ongoing
                        @if($accept->status == 2)
                            <a href="{{ url('order/completed/merchant-confirm/order_id/'.$accept->id.'/merchant_id/'.$accept->merchant_id) }}" title="Click Complete Button If Product has been Successfully Delivered">Complete</a>
                            <a href="{{ url('merchant-review/order_id/'.$accept->id.'/merchant_id/'.$accept->merchant_id.'/runner_id/'.$accept->runner_ID) }}" title="Review This Runner">Review</a>
                        @endif
                    </td>

                </tr>
            @endforeach
        </table>
    </div>
</div>
<!----Main Area---->

</div>
</div>
</div>
</div>
@endsection