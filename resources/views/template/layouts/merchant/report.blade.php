@extends('template.layouts.master')
@section('merchant-report')
    @include('template.layouts.merchant.sidebar')
    <!----Main Area---->
    <div class="col-md-9 col-sm-9 col-xs-12">
        {{--Page Name Header--}}
        <div class="dashboard-level">
            <div class="linkup">
                <ul>
                    <li><a href="#l">home</a></li>
                    <li><a href="#" class="active">blog</a></li>
                </ul>
            </div>
        </div>
        {{--Page Name Header--}}
        <div class="main-table-area">

            <form class="form-inline text-center search-form">
                <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" class="form-control" id="" placeholder="Jane Doe">
                </div>
                <button type="submit" class="btn btn-default">Send invitation</button>
            </form>
            <div class="">
                <table>
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                    </tr>
                    <tr>
                        <td>Name:</td>
                        <td>Ahmed Safa</td>
                    </tr>
                    <tr>
                        <td>Phone:</td>
                        <td>01515243401</td>
                    </tr>
                    <tr>
                        <td>Adderess:</td>
                        <td>201, Sir A F Rahman Hall, Dhaka</td>
                    </tr>
                    <tr>
                        <td>Name:</td>
                        <td>Ahmed Safa</td>
                    </tr>
                    <tr>
                        <td>Order:</td>
                        <td>10</td>
                    </tr>
                    <tr>
                        <td>Joined:</td>
                        <td>30/12/2019</td>
                    </tr>
                </table>
                <form class="search-form">
                    <div class="form-group">
                        <label for="">Report</label>
                        <textarea class="form-control" id="" name="" rows="5"></textarea>
                    </div>
                    <button type="button" class="btn">Report</button>
                </form>
            </div>
        </div>
    </div>
    <!----Main Area---->
    </div>
    </div>
    </div>
    </div>
@endsection