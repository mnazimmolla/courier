@extends('template.layouts.master')
@section('merchant-edit-request')
    @include('template.layouts.merchant.sidebar')
    <!----Main Area---->
    <div class="col-md-9 col-sm-9 col-xs-12">
        {{--Page Name Header--}}
        <div class="dashboard-level">
            <div class="linkup">
                <ul>
                    <li><a href="#l">home</a></li>
                    <li><a href="#" class="active">blog</a></li>
                </ul>
            </div>
        </div>
        {{--Page Name Header--}}


        <div class="our-form">
            <div class="our-form-title text-center">
                <span>Edit Request</span>
            </div>
            <div class="row">
                <div class="col-md-3 col-xs-12 col-sm-3"></div>
                <div class="col-md-6 col-xs-12 col-sm-6">
                    <form action="{{ url('merchant/edit-request/'.$request->id) }}" method="post">
                        <div class="form-group">
                            <label for="pick_up">Pickup Point</label>
                            <input type="text" id="pick_up" name="pick_up" class="form-control" value="{{ $request->pick_up }}">
                        </div>
                        <div class="form-group">
                            <label for="pick_up_address">Pickup Address</label>
                            <textarea name="pick_up_address" id="pick_up_address" rows="3" class="form-control">{{ $request->pick_up_address }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="destination">Destination</label>
                            <input type="text" id="destination" name="destination" class="form-control" value="{{ $request->destination }}">
                        </div>
                        <div class="form-group">
                            <label for="destination_address">Destination Address</label>
                            <textarea name="destination_address" id="destination_address" placeholder="" rows="3" class="form-control">{{ $request->destination_address }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="expected_response">Expected Response Time</label>
                            <input type="time" id="expected_response" name="expected_response" class="form-control" value="{{ $request->expected_response }}">
                        </div>
                        <div class="form-group">
                            <label for="weight">Weight</label>
                            <input type="text" id="weight" name="weight" class="form-control" value="{{ $request->weight }}">
                        </div>
                        <div class="form-group">
                            <label for="payment">Payment</label>
                            <input type="number" id="payment" name="payment" class="form-control" value="{{ $request->payment }}">
                        </div>
                        <div class="form-group">
                            <label for="product_details">Product Details</label>
                            <textarea name="product_details" id="product_details" placeholder="Product Details" class="form-control" rows="3">{{ $request->product_details }}</textarea>
                        </div>
                        <input type="hidden" name="id" id="id" value="{{ $request->id }}">
                        <input type="hidden" name="_token" id="_token" value="{{ Session::token() }}">
                        <button type="submit" class="btn btn-info">Update</button>
                    </form>
                </div>
                <div class="col-md-3 col-xs-12 col-sm-3"></div>
            </div>
        </div>
    </div>
    <!----Main Area---->
    </div>
    </div>
    </div>
    </div>
@endsection

