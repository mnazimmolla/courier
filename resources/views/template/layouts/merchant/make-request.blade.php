@extends('template.layouts.master')
@section('merchant-make-request')
    @include('template.layouts.merchant.sidebar')
    <!----Main Area---->
    <script src="{{ asset('assets/js/vendor/jquery-1.12.4.min.js')  }}"></script>
    <script>
        $( function() {
            var location = [
             @foreach($locations as $key => $value)
                 "{{ $value->area }}",
             @endforeach
            ];
            $( "#pick_up" ).autocomplete({
                source: location
            });
            $( "#destination" ).autocomplete({
                source: location
            });
        } );
    </script>
    <div class="col-md-9 col-sm-9 col-xs-12">
        {{--Page Name Header--}}
        <div class="dashboard-level">
            <div class="linkup">
                <ul>
                    <li><a href="#l">home</a></li>
                    <li><a href="#" class="active">blog</a></li>
                </ul>
            </div>
        </div>
        {{--Page Name Header--}}
        <div class="our-form">
            <div class="our-form-title text-center">
                <span>Make Request</span>
            </div>
            <div class="row">
                <div class="col-md-3 col-xs-12 col-sm-3"></div>
                <div class="col-md-6 col-xs-12 col-sm-6">
                    <form action="{{ url('merchant/make-request') }}" method="post">
                        <div class="form-group">
                            <label for="pick_up">Pickup Point</label>
                            <input type="text" id="pick_up" name="pick_up" class="form-control{{ $errors->has('pick_up') ? ' is-invalid' : '' }}" placeholder="Pickup Point">
                            @if ($errors->has('pick_up'))
                                <span class="text-danger">
                                        <strong>{{ $errors->first('pick_up') }}</strong>
                                 </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="pick_up_address">Pickup Address</label>
                            <textarea name="pick_up_address" id="pick_up_address" rows="3" class="form-control{{ $errors->has('pick_up_address') ? ' is-invalid' : '' }}"></textarea>
                            @if ($errors->has('pick_up_address'))
                                <span class="text-danger">
                                        <strong>{{ $errors->first('pick_up_address') }}</strong>
                                 </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="destination">Destination</label>
                            <input type="text" id="destination" name="destination" class="form-control{{ $errors->has('destination') ? ' is-invalid' : '' }}" placeholder="Destination">
                            @if ($errors->has('destination'))
                                <span class="text-danger">
                                        <strong>{{ $errors->first('destination') }}</strong>
                                 </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="destination_address">Destination Address</label>
                            <textarea name="destination_address" id="destination_address" placeholder="" rows="3" class="form-control{{ $errors->has('destination_address') ? ' is-invalid' : '' }}"></textarea>
                            @if ($errors->has('destination_address'))
                                <span class="text-danger">
                                        <strong>{{ $errors->first('destination_address') }}</strong>
                                 </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="expected_response">Expected Response Time</label>
                            <input type="time" id="expected_response" name="expected_response" class="form-control{{ $errors->has('expected_response') ? ' is-invalid' : '' }}" placeholder="Expected Response Time">
                            @if ($errors->has('expected_response'))
                                <span class="text-danger">
                                        <strong>{{ $errors->first('expected_response') }}</strong>
                                 </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="weight">Weight</label>
                            <input type="text" id="weight" name="weight" class="form-control{{ $errors->has('weight') ? ' is-invalid' : '' }}" placeholder="Weight">
                            @if ($errors->has('weight'))
                                <span class="text-danger">
                                        <strong>{{ $errors->first('weight') }}</strong>
                                 </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="payment">Payment</label>
                            <input type="number" id="payment" name="payment" class="form-control{{ $errors->has('payment') ? ' is-invalid' : '' }}" placeholder="Payment">
                            @if ($errors->has('payment'))
                                <span class="text-danger">
                                        <strong>{{ $errors->first('payment') }}</strong>
                                 </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="product_details">Product Details</label>
                            <textarea name="product_details" id="product_details" placeholder="Product Details" class="form-control{{ $errors->has('product_details') ? ' is-invalid' : '' }}" rows="3"></textarea>
                            @if ($errors->has('product_details'))
                                <span class="text-danger">
                                        <strong>{{ $errors->first('product_details') }}</strong>
                                 </span>
                            @endif
                        </div>
                        <input type="hidden" name="merchant_id" id="merchant_id" value="{{ Auth::user()->id }}">
                        <input type="hidden" name="_token" id="_token" value="{{ Session::token() }}">
                        <button type="submit" class="btn btn-info">Submit</button>
                    </form>
                </div>
                <div class="col-md-3 col-xs-12 col-sm-3"></div>
            </div>
        </div>
    </div>
    <!----Main Area---->



    </div>
    </div>
    </div>
    </div>


@endsection

