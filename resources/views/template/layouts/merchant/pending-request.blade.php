@extends('template.layouts.master')
@section('pending-request')
    @include('template.layouts.merchant.sidebar')
    <!----Main Area---->

    {{--<style>--}}
        {{--.ui-autocomplete {--}}
            {{--z-index: 999999999;--}}
        {{--}--}}
    {{--</style>--}}
    <div class="col-md-9 col-sm-9 col-xs-12">
        {{--Page Name Header--}}
        <div class="dashboard-level">
            <div class="linkup">
                <ul>
                    <li><a href="#l">home</a></li>
                    <li><a href="#" class="active">blog</a></li>
                </ul>
            </div>
        </div>
        {{--Page Name Header--}}

        <div class="main-table-area">
            <table class="table-striped">
                <tr>
                    <th>Order No</th>
                    <th>Pickup Point</th>
                    <th>Destination</th>
                    <th>Weight</th>
                    <th>Payment</th>
                    <th>Action</th>
                </tr>

                @if(Session::has('empty'))
                    <div class="alert alert-danger">{{ Session::get('empty') }}</div>
                @else

                @foreach($resources['data'] as $request)
                <tr>
                    <td>#{{ $request->order_id }}</td>
                    <td>{{ $request->pick_up }}</td>
                    <td>{{ $request->destination }}</td>
                    <td>{{ $request->weight }}</td>
                    <td>{{ $request->payment }}BDT</td>
                    <td>
                        <a href="{{ url('/merchant/'.Auth::user()->id.'/edit-request/'.$request->id) }}">Edit</a> ||
                        <a href="{{ url('/merchant/'.Auth::user()->id.'/delete-request/'.$request->id) }}">Delete</a>
                    </td>
                </tr>
                @endforeach
                @endif
            </table>
        </div>
    </div>
    <!----Main Area---->

    {{--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>--}}
    {{--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}
    {{--<script>--}}
        {{--$( function() {--}}
            {{--var location = [--}}
                {{--@foreach($resources['location'] as $key => $value)--}}
                    {{--"{{ $value->area }}",--}}
                {{--@endforeach--}}
            {{--];--}}
            {{--$( "#pick_up" ).autocomplete({--}}
                {{--source: location--}}
            {{--});--}}
            {{--$( "#destination" ).autocomplete({--}}
                {{--source: location--}}
            {{--});--}}
        {{--} );--}}

    {{--</script>--}}

    </div>
    </div>
    </div>
    </div>
@endsection