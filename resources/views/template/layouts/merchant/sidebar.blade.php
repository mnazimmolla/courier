<div class="dash-area">
    <div class="dashboard-area">
        @if(Auth::check())
        <p>Welcome, <span>{{ Auth::user()->name }}</span></p>
        @endif
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="dash-padding">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="dash-nav">
                        <div class="list-group panel">
                            <a href="{{ url('/merchant/profile') }}" class="list-group-item">My Profile</a>
                        </div>
                        <div class="list-group panel">
                            <a href="{{ url('/merchant/make-request')  }}" class="list-group-item">Make Request</a>
                        </div>
                        <div class="list-group panel">
                            <a href="{{ url('/merchant/pending-request') }}" class="list-group-item">Pending
                                Requests</a>
                        </div>
                        <div class="list-group panel">
                            <a href="{{ url('/merchant/accepted-request') }}" class="list-group-item">Accepted
                                Requests</a>
                        </div>
                        <div class="list-group panel">
                            <a href="{{ url('/merchant/complete-request') }}" class="list-group-item">Complete
                                Requests</a>
                        </div>
                        <div class="list-group panel">
                            <a href="#" class="list-group-item">Review</a>
                        </div>
                        <div class="list-group panel">
                            @if(Auth::check())
                            <a href="{{ url('/merchant/edit-profile/'.Auth::user()->id ) }}"
                               class="list-group-item">Edit Profile</a>
                            @endif
                        </div>
                        <div class="list-group panel">
                            <a href="{{ url('/merchant/report') }}" class="list-group-item">Report to Admin</a>
                        </div>
                    </div>
                </div>