@extends('template.layouts.master')
@section('orders')
    @if(Auth::check())
        @if( Auth::user()->role === 4 )
        @include('template.layouts.admin.sidebar')
        @elseif(Auth::user()->role === 3 )
        @include('template.layouts.moderator.sidebar')
        @elseif(Auth::user()->role === 2 )
        @include('template.layouts.merchant.sidebar')
        @elseif(Auth::user()->role === 1 )
        @include('template.layouts.runner.sidebar')
        @endif
    @endif
    <!----Main Area---->
    <div class="col-md-9 col-sm-9 col-xs-12">
        {{--Page Name Header--}}
        <div class="dashboard-level">
            <div class="linkup">
                <ul>
                    <li><a href="#l">home</a></li>
                    <li><a href="#" class="active">blog</a></li>
                </ul>
            </div>
        </div>
        {{--Page Name Header--}}

        <div class="main-table-area">
            <div class="container-fluid order-lists">
                <div class="row">
                    <div class="col-md-12 text-center ">
                        <h3> Request lists</h3>
                    @if(count($orders) == 0)
                    <p class="alert alert-danger">Currently You don't have any Available Request.</p>

                    </div>
                    @else

                    <div class="col-md-12 col-sm-12">
                        <div class="order-destination">
                            <form class="form-inline" action="{{ url('/search') }}" method="#">
                                <div class="form-group">
                                    <label for="picup_up">Picup Point</label>
                                    <input type="text" class="form-control" id="picup_up" name="picup_up"
                                           placeholder="picup point">
                                </div>
                                <div class="form-group">
                                    <label for="destination">Destination</label>
                                    <input type="text" class="form-control" id="destination" name="destination"
                                           placeholder="destination">
                                </div>
                                <div class="form-group">
                                    <label for="weight">Weight</label>
                                    <input type="text" class="form-control" id="weight" name="weight"
                                           placeholder="destination">
                                </div>
                                <button type="submit" class="btn btn-default">Search</button>
                            </form>
                        </div>
                    </div>

                </div>
                <div class="row">
                    @foreach($orders as $order)
                    <div class="col-md-12 col-sm-12">
                        <div class="border">
                            <div class="col-md-7 col-sm-6 col-xs-12">
                                <div class="order">
                                    <div class="order-details">
                                        <p><span>Order ID :</span> {{ $order->order_id }} </p>
                                        <p>{{ $order->product_details }}</p>
                                        <p>Area : {{ $order->destination }}</p>
                                        <p>Address : {{ $order->destination_address }}</p>
                                        <p>Pickup Area : {{ $order->pick_up }}</p>
                                        <p>Pickup Address : {{ $order->pick_up_address }}</p>
                                        <ul>
                                            <li><a title="Phone"><i class="fa fa-phone"></i></a> 0{{ $order->phone }}</li>
                                            <li><a title="Expected Response Time"><i class="fa fa-motorcycle"></i></a> {{ $order->expected_response }} </li>
                                            <li><a title="Location"><i class="fa fa-map-marker"></i></a> Dhaka</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 col-sm-6 col-xs-12">
                                <div class="order-button">
                                    <a href="{{ url('/order/'.$order->merchant_id.'/'.$order->id) }}">View Order</a>
                                    <a href="{{ url('/order/accept/'.$order->merchant_id.'/'.$order->id) }}">Accept Request</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endif
            </div>
        </div>
    </div>
    <!----Main Area---->
    </div>
    </div>
    </div>
    </div>
@endsection
