@extends('template.layouts.master')
@section('order')
    @if(Auth::check())
        @if( Auth::user()->role === 2 )
            @include('template.layouts.merchant.sidebar')
        @endif
    @endif
    <!----Main Area---->
    <div class="col-md-9 col-sm-9 col-xs-12">
        {{--Page Name Header--}}
        <div class="dashboard-level">
            <div class="linkup">
                <ul>
                    <li><a href="#l">home</a></li>
                    <li><a href="#" class="active">blog</a></li>
                </ul>
            </div>
        </div>
        {{--Page Name Header--}}
        <div class="main-table-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="main-single-order">
                            <span>Order ID : <b>#{{ $order_details['order']->order_id }}</b></span>
                            <p>Requested by : {{ $order_details['order']->name }} || {{ $order_details['order']->created_at }}</p>

                        </div>
                        <div class="order-payment-area">
                            <ul>
                                <li><i class="fa fa-money"></i> Payment:<span> {{ $order_details['order']->payment }}Tk</span></li>
                                <li><i class="fa fa-clock-o"></i> Expected Response :<span> {{ $order_details['order']->expected_response }}</span></li>
                                <li><i class="fa fa-suitcase"></i> Weight:<span> {{ $order_details['order']->weight }}</span></li>
                            </ul>
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="print">
                            <a href="javascript:window.print();" class="btn btn-primary">Print Request</a>
                            @if(Auth::check())
                            <a href="{{ url('/order/accept/'.Auth::user()->id.'/'.$order_details['order']->id) }}" class="btn btn-success">Accept Request</a><br>
                            @else
                                <a href="#" class="btn btn-success" data-toggle="modal" data-target="#runnerModel">Accept Request</a>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="order-message">
                            <p><strong>Details</strong></p><br>
                            <p>Hi there,</p>
                            <p>{{ $order_details['order']->product_details }}</p>
                            <p><strong>Pickup Point</strong> : {{ $order_details['order']->pick_up_address }}</p>
                            <p><strong>Destination </strong> : {{ $order_details['order']->destination_address }}</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="order-client-details">
                            <ul>
                                <li><strong>Mirpur,Bangladesh</strong></li>
                                <li>Bend 07:59PM</li>
                                <li><strong>Total Requests : {{ $order_details['total']->total }}</strong></li>
                                <li>Available request : {{ $order_details['available']->available }}</li>
                                <li>
                                    @if($order_details['total_spent']->spent == null)
                                    <strong>Not Spent Yet.</strong>
                                    @else
                                    <strong>{{ $order_details['total_spent']->spent }}TK Total Spent</strong>
                                    @endif
                                </li>
                                <li>{{ $order_details['total_hire']->hires }} Hires, {{ $order_details['available']->available }} Active</li>
                                <li>Member Since : {{ substr($order_details['order']->created_at,0,10) }}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!----Main Area---->
    </div>
    </div>
    </div>
    </div>


@endsection