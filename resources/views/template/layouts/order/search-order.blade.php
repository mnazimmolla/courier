@extends('template.layouts.master')
@section('search-order')
    <!----Main Area---->

    <div class="col-md-9 col-sm-9 col-xs-12">
        {{--Page Name Header--}}
        <div class="dashboard-level">
            <div class="linkup">
                <ul>
                    <li><a href="#l">home</a></li>
                    <li><a href="#" class="active">blog</a></li>
                </ul>
            </div>
        </div>
        {{--Page Name Header--}}
        <div class="main-table-area">
            <div class="container-fluid order-lists">
                <div class="row">
                    <div class="col-md-12 text-center ">
                        <h3> Request lists</h3>
                    </div>

                        <div class="col-md-12 col-sm-12">
                            <div class="order-destination">
                                <form class="form-inline" method="POST">
                                    <div class="form-group">
                                        <label for="pick_up">Picup Point</label>
                                        <input type="text" class="form-control" id="pick_up" name="pick_up"
                                               placeholder="picup point">
                                    </div>
                                    <div class="form-group">
                                        <label for="destination">Destination</label>
                                        <input type="text" class="form-control" id="destination" name="destination"
                                               placeholder="destination">
                                    </div>
                                    <button type="submit" class="btn btn-default" name="search" id="search">Search</button>
                                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                                </form>
                            </div>
                        </div>

                </div>

                <div class="row">

                </div>

            </div>
        </div>
    </div>
    <!----Main Area---->


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#search').click(function () {
                var pickup = $('#pick_up').val();
                var destination = $('#destination').val();
                var weight = $('#weight').val();

                $.ajax({
                    url : "{{ url('/search') }}",
                    method : "POST",
                    dataType : "JSON",
                    data : { 'pick_up' : pickup,'destination' : destination},

                    success:function (response) {
                        console.log(response);
                    },
                    error:function () {
                        console.log("There was an error.");
                    }
                });
            });
        });

    </script>
    </div>
    </div>
    </div>
    </div>
@endsection
