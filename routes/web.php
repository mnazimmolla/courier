<?php

//Root
Route::get('/', function (){
    return view('template.layouts.master');
});

//Merchant Profile

Route::get('/merchant/signup', 'MerchantController@create');
Route::post('/merchant/signup', 'MerchantController@store');
Route::post('/merchant/login', 'MerchantController@login');
Route::get('/merchant/profile', 'MerchantController@index')->middleware('auth');
Route::post('/merchant/profile/update/{id}', 'MerchantController@update')->middleware('auth');
Route::get('/merchant/', 'MerchantController@index')->middleware('auth');
Route::get('/merchant/logout', 'MerchantController@logout')->middleware('auth');

//End Merchant Profile

/*
 *Merchant Make Request
 */
Route::get('merchant/make-request', 'RequestController@create')->middleware('auth');
Route::post('merchant/make-request', 'RequestController@store')->middleware('auth');
Route::get('/merchant/pending-request', 'RequestController@index')->middleware('auth');
Route::get('/merchant/{user_id}/edit-request/{id}', 'RequestController@show')->middleware('auth');
Route::post('/merchant/edit-request/{id}', 'RequestController@update')->middleware('auth');
Route::get('/merchant/{user_id}/delete-request/{id}', 'RequestController@destroy')->middleware('auth');
Route::get('/merchant/accepted-request', 'RequestController@acceptRequests')->middleware('auth');
Route::get('/merchant/complete-request', 'RequestController@merchantCompleteRequests')->middleware('auth');

// End Merchant Make Request


//Runner Profile

Route::get('/runner/signup', 'RunnerController@create');
Route::post('/runner/signup', 'RunnerController@store');
Route::post('/runner/login', 'RunnerController@login');
Route::get('/runner/profile', 'RunnerController@index')->middleware('auth');
Route::post('/runner/profile/update/{id}', 'RunnerController@update')->middleware('auth');

//End Runner Profile

//Runner Accept Request

Route::get('/runner/accepted-request', 'RequestController@runnerAccepted')->middleware('auth');
Route::get('/requests/merchant/{id}', 'RequestController@requestedMerchantProfile')->middleware('auth');
Route::get('/runner/complete-request', 'RequestController@runnerCompleteRequests')->middleware('auth');
Route::get('/requests/runner/{id}', 'RequestController@acceptedRunnerProfile')->middleware('auth');

//End Runner Accept Request

//Orders
Route::get('/orders', 'RequestController@orders');
Route::get('/order/{merchant_id}/{id}', 'RequestController@order');
Route::get('/order/accept/{runner_id}/{order_id}', 'RequestController@acceptOrder');
Route::get('order/completed/runner/order_id/{order_id}/merchant_id/{merchant_id}', 'RequestController@runnerOrderComplete')->middleware('auth');
Route::get('order/completed/merchant-confirm/order_id/{order_id}/merchant_id/{merchant_id}', 'RequestController@merchantOrderCompleteConfirmation')->middleware('auth');


//End Orders

//Review
Route::get('merchant-review/order_id/{order_id}/merchant_id/{merchant_id}/runner_id/{runner_id}', 'ReviewController@create')->middleware('auth');

Route::post('/merchant/feedback', 'ReviewController@store')->middleware('auth');

Route::get('/runner/reviews', 'ReviewController@index')->middleware('auth');
//End Review

//Search

Route::get('/search', 'RequestController@searchForRunnerForm');
Route::post('/search', 'RequestController@searchForRunner');

Route::get('/search-runner', 'RunnerController@searchRunnerForm');
Route::post('/search-runner', 'RunnerController@searchRunner');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

