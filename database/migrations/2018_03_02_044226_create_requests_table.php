<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('order_id');
            $table->integer('merchant_id');
            $table->integer('runner_id')->nullable();
            $table->text('product_details');
            $table->string('weight')->nullable();
            $table->string('pick_up');
            $table->text('pick_up_address')->nullable();
            $table->string('destination');
            $table->text('destination_address')->nullable();
            $table->string('expected_response')->nullable();
            $table->integer('payment');
            $table->integer('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
